package com.crossfield.controller;

import com.crossfield.rates.domain.Instrument;
import com.crossfield.rates.provider.MockMarketProvider;
import com.crossfield.rates.provider.ProviderManager;
import com.crossfield.rates.vwap.VwapProcessor;


public class Main {

	public static void main(final String[] args) throws InterruptedException {
		final ProviderManager manager = new ProviderManager(new VwapProcessor());
		manager.addProviders(new MockMarketProvider("providerA"),
				new MockMarketProvider("providerB"), new MockMarketProvider(
						"providerC"));

		manager.subscribeToAllProviders(Instrument.EURGBP, Instrument.EURUSD,
				Instrument.GBPUSD);

		Thread.sleep(5000);

		manager.unsubscribeAll();

		System.exit(0);
	}

}

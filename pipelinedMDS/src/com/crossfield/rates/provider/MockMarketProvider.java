package com.crossfield.rates.provider;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.crossfield.rates.RateUpdateListener;
import com.crossfield.rates.domain.Instrument;
import com.crossfield.rates.domain.RateUpdate;


public class MockMarketProvider implements MarketProvider {

	private final String name;
	private final ScheduledExecutorService executor = Executors
			.newScheduledThreadPool(10);

	private final ConcurrentHashMap<Instrument, Double> lastMids = new ConcurrentHashMap<Instrument, Double>();

	public MockMarketProvider(final String name) {
		super();
		this.name = name;
	}

	@Override
	public void subscribe(final Instrument instrument,
			final RateUpdateListener listener) {
		executor.schedule(createRateTickTask(instrument, listener),
				randomDelay(), TimeUnit.MILLISECONDS);

	}

	private long randomDelay() {
		return (long) (Math.random() * 1000L);

	}

	private Runnable createRateTickTask(final Instrument instrument,
			final RateUpdateListener listener) {
		return new Runnable() {

			@Override
			public void run() {
				Thread.currentThread().setName("MockMarketData +"+instrument.toString());
				listener.onRateUpdate(randomMockTick(instrument));
				executor.schedule(this, randomDelay(), TimeUnit.MILLISECONDS);

			}

		};
	}

	private RateUpdate randomMockTick(final Instrument instrument) {
		final double mid = rateTick(instrument);
		final long bidVol = (long) (Math.random() * 10000);
		final long askVol = (long) (Math.random() * 10000);
		return new RateUpdate(name, instrument, spread(mid, true), spread(mid,
				false), bidVol, askVol);
	}

	private double spread(final double mid, final boolean isBid) {
		final double spread = mid * 0.01;
		return isBid ? mid - spread : mid + spread;
	}

	private double rateTick(final Instrument instrument) {
		Double price = lastMids.get(instrument);
		if (price == null) {
			price = (Math.random() * 2d);

		} else {
			final double delta = ((Math.random() - 0.5) / 100) * price;
			price = price + delta;
		}
		lastMids.put(instrument, price);
		return price;
	}

	@Override
	public void unsubscribe(final Instrument instrument) {
		// TODO Auto-generated method stub

	}

}

package com.crossfield.rates.provider;

import com.crossfield.rates.RateUpdateListener;
import com.crossfield.rates.domain.Instrument;

public interface MarketProvider {

	void subscribe(final Instrument instrument, final RateUpdateListener listener);

	void unsubscribe(final Instrument instrument);

}

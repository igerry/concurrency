package com.crossfield.rates.provider;

import java.util.ArrayList;
import java.util.List;

import com.crossfield.rates.RateUpdateListener;
import com.crossfield.rates.domain.Instrument;
import com.crossfield.rates.domain.RateUpdate;
import com.crossfield.rates.util.PipelinedExecutor;


public class ProviderManager {

	private final List<MarketProvider> providers = new ArrayList<MarketProvider>();
	private final PipelinedExecutor executor = new PipelinedExecutor();
	private final RateUpdateListener processor;

	public ProviderManager(final RateUpdateListener processor) {
		this.processor = processor;
	}

	public void addProviders(final MarketProvider... marketProviders) {
		for (final MarketProvider provider : marketProviders) {
			providers.add(provider);
		}
	}

	public void subscribeToAllProviders(final Instrument... instruments) {
		for (final Instrument instrument : instruments) {
			for (final MarketProvider provider : providers) {

				provider.subscribe(instrument, new RateUpdateListener() {

					@Override
					public void onRateUpdate(final RateUpdate rateUpdate) {
						offload(rateUpdate);

					}

				});
			}
		}
	}

	private void offload(final RateUpdate rateUpdate) {
		final int hash = rateUpdate.getInstrument().hashCode();
		executor.execute(new Runnable() {

			@Override
			public void run() {
				processor.onRateUpdate(rateUpdate);

			}
		}, hash);

	}

	public void unsubscribeAll() {
		// TODO Auto-generated method stub

	}
}

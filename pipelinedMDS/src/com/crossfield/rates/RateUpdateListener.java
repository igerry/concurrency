package com.crossfield.rates;

import com.crossfield.rates.domain.RateUpdate;


public interface RateUpdateListener {

	void onRateUpdate(RateUpdate rateUpdate);
}

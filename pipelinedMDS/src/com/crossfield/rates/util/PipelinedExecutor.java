package com.crossfield.rates.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class PipelinedExecutor implements Executor {

	private final List<Executor> executors = new ArrayList<Executor>();

	public PipelinedExecutor() {
		this(Runtime.getRuntime().availableProcessors());
	}

	public PipelinedExecutor(final int threads) {
		for (int i = 0; i < threads; i++) {
			executors.add(Executors.newSingleThreadScheduledExecutor());
		}
	}

	public void execute(final Runnable runnable, final int hash) {
		final int index = hash % executors.size();
		executors.get(index).execute(runnable);
	}

	@Override
	public void execute(final Runnable runnable) {
		execute(runnable, 0);

	}
}

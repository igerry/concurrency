package com.crossfield.rates.domain;


public class RateUpdate {

	private final String provider;
	private final Instrument instrument;
	private final double bid;
	private final double ask;
	private final long bidVol;
	private final long askVol;

	public RateUpdate(final String provider, final Instrument instrument,
			final double bid, final double ask, final long bidVol,
			final long askVol) {
		super();
		this.provider = provider;
		this.instrument = instrument;
		this.bid = bid;
		this.ask = ask;
		this.bidVol = bidVol;
		this.askVol = askVol;
	}

	public String getProvider() {
		return provider;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public double getBid() {
		return bid;
	}

	public double getAsk() {
		return ask;
	}

	public double getBidVol() {
		return bidVol;
	}

	public double getAskVol() {
		return askVol;
	}

	@Override
	public String toString() {
		return "RateUpdate [provider=" + provider + ", instrument="
				+ instrument + ", bid=" + bid + ", ask=" + ask + ", bidVol="
				+ bidVol + ", askVol=" + askVol + "]";
	}

}

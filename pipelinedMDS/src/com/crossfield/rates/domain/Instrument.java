package com.crossfield.rates.domain;


public class Instrument {
	String ccyPair;

	public final static Instrument EURUSD = new Instrument("EURUSD");
	public final static Instrument GBPUSD = new Instrument("GBPUSD");
	public final static Instrument EURGBP = new Instrument("EURGBP");

	public Instrument(final String ccyPair) {
		super();
		this.ccyPair = ccyPair;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ccyPair == null) ? 0 : ccyPair.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Instrument other = (Instrument) obj;
		if (ccyPair == null) {
			if (other.ccyPair != null)
				return false;
		} else if (!ccyPair.equals(other.ccyPair))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Instrument [ccyPair=" + ccyPair + "]";
	}

}

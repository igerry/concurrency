package com.crossfield.rates.vwap;

import java.util.concurrent.ConcurrentHashMap;

import com.crossfield.rates.RateUpdateListener;
import com.crossfield.rates.domain.Instrument;
import com.crossfield.rates.domain.RateUpdate;


public class VwapProcessor implements RateUpdateListener {

	private final ConcurrentHashMap<Instrument, VwapData> vwaps = new ConcurrentHashMap<Instrument, VwapData>();

	@Override
	public void onRateUpdate(final RateUpdate rateUpdate) {
		VwapData data = new VwapData(rateUpdate.getInstrument());
		final VwapData existing = vwaps.putIfAbsent(rateUpdate.getInstrument(),
				data);
		data = existing == null ? data : existing;
		data.add(rateUpdate.getBid(), rateUpdate.getBidVol(),
				rateUpdate.getAsk(), rateUpdate.getAskVol());
		System.out.println(data + " " + rateUpdate + " ");

	}
}

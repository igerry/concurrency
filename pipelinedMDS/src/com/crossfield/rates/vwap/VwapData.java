package com.crossfield.rates.vwap;

import com.crossfield.rates.domain.Instrument;


public class VwapData {

	private final Instrument instrument;
	private double cumBidPriceVol = 0;
	private double cumBidVol = 0;

	private double cumAskPriceVol = 0;
	private double cumAskVol = 0;

	public VwapData(final Instrument instrument) {
		super();
		this.instrument = instrument;
	}

	public void add(final double bidPrice, final double bidVolume,
			final double askPrice, final double askVolume) {
		cumBidVol = cumBidVol + bidVolume;
		cumBidPriceVol = cumBidPriceVol + (bidVolume * bidPrice);

		cumAskVol = cumAskVol + askVolume;
		cumAskPriceVol = cumAskPriceVol + (askVolume * askPrice);
	}

	public double getBidVWAP() {
		return cumBidPriceVol / cumBidVol;
	}

	public double getAskVWAP() {
		return cumAskPriceVol / cumAskVol;
	}

	@Override
	public String toString() {
		return "VwapData [instrument=" + instrument + ", bidVWAP="
				+ getBidVWAP() + ", askVWAP=" + getAskVWAP() + "]";
	}

}

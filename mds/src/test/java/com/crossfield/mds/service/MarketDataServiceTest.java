package com.crossfield.mds.service;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;







import com.crossfield.mds.domain.Instrument;
import com.crossfield.mds.provider.MockMarketData;
import com.crossfield.mds.provider.ProviderManager;
import com.crossfield.mds.service.MarketDataService;

public class MarketDataServiceTest {

	private ProviderManager manager;
	private final static long MaxPublishInterval=1000;
	private final static long MaxPublish=Long.MAX_VALUE;
	private final static short MaxExecutors = 4;
	
	
	@Before
	public void setUp() throws Exception {
		manager = new ProviderManager();
	}

	
	@Test

	public void runForDuration() {
		
		manager.addProviders(
				new MockMarketData("M1",MaxExecutors,MaxPublishInterval,MaxPublish),
				new MockMarketData("M2",MaxExecutors,MaxPublishInterval,MaxPublish), 
				new MockMarketData("M3",MaxExecutors,MaxPublishInterval,MaxPublish));	
		
		try (MarketDataService service = new MarketDataService(manager)) {	
			service.subscribeForInstrument(Instrument.EURGBP, Instrument.EURUSD,Instrument.USDCAD);
			TimeUnit.MINUTES.sleep(1);
		} catch (final Exception ex) {
			fail("Not expected for this test :" + ex );
		}		
	}

	
	@Test
	@Ignore
	public void limitedPublicationOnSingleInstrument() {
		
		manager.addProviders(
				new MockMarketData("M1",MaxExecutors,100, 50),
				new MockMarketData("M2",MaxExecutors,100, 50),
				new MockMarketData("M3",MaxExecutors,100, 50));
		
		try (MarketDataService service = new MarketDataService(manager)) {	
			service.subscribeForInstrument(Instrument.EURGBP);
			TimeUnit.SECONDS.sleep(5);
			service.unsubscribeAll();
		} catch (final Exception ex) {
			fail("Not expected for this test :" + ex );
		}		
	}

	
}

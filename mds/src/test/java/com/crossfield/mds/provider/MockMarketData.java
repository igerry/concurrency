package com.crossfield.mds.provider;



import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.crossfield.mds.domain.Instrument;
import com.crossfield.mds.domain.Rate;
import com.crossfield.mds.provider.MarketProvider;
import com.crossfield.mds.provider.Processor;


public class MockMarketData implements MarketProvider {

	private int published;
	private final long maxToPublish;
	private final ScheduledExecutorService executor;
	private final String id;
	private final long publishInterval;
	private final Map<Instrument, Double> lastMids = new ConcurrentHashMap<>();

	public MockMarketData(final String id, final int maxExecutorSz,long publishInterval,long maxToPublish) {
		this.executor = Executors.newScheduledThreadPool(maxExecutorSz);
        this.id = id;
        this.publishInterval=publishInterval;
        this.maxToPublish=maxToPublish;
	}

	@Override
	public void subscribe(final Instrument instrument,
			final Processor processor) {
		System.out.println(
				String.format("Subscription to provider started.  ID:%s for instrument %s", id, instrument));
		executor.schedule(createRateTickTask(instrument, processor),
				createDelay(), TimeUnit.MILLISECONDS);

	}


	private Runnable createRateTickTask(final Instrument instrument,
			final Processor processor) {
		return new Runnable() {
			@Override
			public void run() {
				while(published<maxToPublish){
				   processor.onRateChange(tick(instrument));
				   published++;
				   executor.schedule(this, createDelay(), TimeUnit.MILLISECONDS);
				   try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
			}
		};
	}

	private Rate tick(final Instrument instrument) {
		final double mid = rateTick(instrument);
		final long bidVol = (long) (Math.random() * 10000);
		final long askVol = (long) (Math.random() * 10000);
		return new Rate(id,instrument.toString(), spread(mid, true), spread(mid,
				false), bidVol, askVol);
	}

	private double spread(final double mid, final boolean isBid) {
		final double spread = mid * 0.01;
		return isBid ? mid - spread : mid + spread;
	}

	private double rateTick(final Instrument instrument) {
		Double price = lastMids.get(instrument);
		if (price == null) {
			price = (Math.random() * 2d);

		} else {
			final double delta = ((Math.random() - 0.5) / 100) * price;
			price = price + delta;
		}
		lastMids.put(instrument, price);
		return price;
	}

	private long createDelay() {
		return (long) (Math.random() * publishInterval);

	}

	
	@Override
	public void unsubscribe(Instrument instrument) {
        System.out.println("Unsubsribed "+instrument + "  " + this.id);
        executor.shutdown();       
	}

}

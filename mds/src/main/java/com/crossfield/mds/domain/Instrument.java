package com.crossfield.mds.domain;

// For prototype keep object model very simple
public enum Instrument {
	
	// The majors
	EURUSD("EURUSD"),EURGBP("EURGBP"),USDJPY("USDJPY"),GBPUSD("GBPUSD"),USDCAD("USDCAD"), USDCHF("USDCHF");
	private final String ccyPair;

	Instrument(final  String ccyPair) {
	   this.ccyPair = ccyPair;
	}
	
	@Override
	public String toString(){
		return ccyPair;
	}
}

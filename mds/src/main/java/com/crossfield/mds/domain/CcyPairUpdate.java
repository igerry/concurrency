package com.crossfield.mds.domain;


public class CcyPairUpdate {

	private Rate rate;
		
	public Rate getRate() {
		return rate;
	}

	public void setRate(final Rate rate) {
		this.rate = rate;
	}


	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(100);
		if (rate!=null){
		   sb.append("[provider=").append(rate.getProvider());
		   sb.append("instrument=").append(rate.getInstrument());
		   sb.append("]");
		}
		return sb.toString();
	}

}

package com.crossfield.mds.domain;


// Encapsulate Rate information in immutable class
public final class Rate {

	private final String provider;
	private final String instrument;
	private final double bid;
	private final double ask;
	private final long bidVol;
	private final long askVol;

	
	public Rate(final String provider,final String instrument, final double bid, final double ask, final long bidVol,
			final long askVol) {
		this.provider=provider;
		this.instrument=instrument;
		this.bid = bid;
		this.ask = ask;
		this.bidVol = bidVol;
		this.askVol = askVol;
	}

	
	public double getBid() {
		return bid;
	}

	public double getAsk() {
		return ask;
	}

	public double getBidVol() {
		return bidVol;
	}

	public double getAskVol() {
		return askVol;
	}
	
	public String getProvider() {
		return provider;
	}


	public String getInstrument() {
		return instrument;
	}
	
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder(200);
		sb.append("[Rate ");
		sb.append("  Provider=").append(provider);
		sb.append(", instrument=").append(instrument);
		sb.append(", ask=").append(ask);
		sb.append(", bid=").append(bid);
		sb.append("]");
		return sb.toString();
	}
	

}

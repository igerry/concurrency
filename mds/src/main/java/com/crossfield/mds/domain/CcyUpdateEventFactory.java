package com.crossfield.mds.domain;

import com.lmax.disruptor.EventFactory;



public class CcyUpdateEventFactory implements EventFactory<CcyPairUpdate> {
	
	public static final CcyUpdateEventFactory INSTANCE = new CcyUpdateEventFactory();

	public CcyPairUpdate newInstance(){
	    return new CcyPairUpdate();
	 }
}

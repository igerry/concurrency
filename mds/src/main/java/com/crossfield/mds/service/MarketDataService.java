package com.crossfield.mds.service;




import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.crossfield.mds.domain.CcyUpdateEventFactory;
import com.crossfield.mds.domain.Instrument;
import com.crossfield.mds.measures.Measure;
import com.crossfield.mds.measures.VwapCalculator;
import com.crossfield.mds.provider.MarketEventProcessor;
import com.crossfield.mds.provider.MockMarketData;
import com.crossfield.mds.provider.ProviderManager;
import com.crossfield.mds.publisher.PrintWriterPublisher;


/**
 * Associate providers with business logic processors
 */
public class MarketDataService implements AutoCloseable {

	private volatile boolean publish;
	private final ProviderManager manager;
	private final Map<Instrument,Measure> results = new ConcurrentHashMap<>();
	
	
	public MarketDataService(final ProviderManager manager){
		this.manager= manager;
	}
		
	public void subscribeForInstrument(final Instrument... instruments){
		
		for (final Instrument instrument : instruments) {
			manager.subscribe(createProcessor(instrument,results), instrument);	
		}
	}
	
	
	public void unsubscribeAll(){
		if(manager!=null){
		    manager.unsubscribeAll();
		}
	}

	
	public void startPublisher(){
		final ScheduledExecutorService scheduledPublisher = Executors.newScheduledThreadPool(2);
		
		long periodBetweenPublications = 5000; 
		long initialDelay = 1000;
 
		scheduledPublisher.scheduleAtFixedRate( new Runnable(){
			@Override
			public void run(){
				Thread.currentThread().setName   ("Publisher");
				//Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
				 new PrintWriterPublisher(results).publish();
			}
		}, initialDelay, periodBetweenPublications,TimeUnit.MILLISECONDS);
	}
	
	
	private MarketEventProcessor createProcessor(final Instrument i, Map<Instrument,Measure> results){
		final MarketEventProcessor processor =new MarketEventProcessor(i, new VwapCalculator(results, i), 
    			new CcyUpdateEventFactory(), Executors.newCachedThreadPool());
		processor.init();
		return processor;
	}
	
	@Override
	public void close() throws Exception {
		
		// Do some sensible clean-up and production alerting
		unsubscribeAll();
	}
	
	
	public static void main(String[] args){
		
		
	    ProviderManager manager = new ProviderManager();
		long MaxPublishInterval=5000;
		long MaxPublish=Long.MAX_VALUE;
		short MaxExecutors = 4;
	    System.out.println("Publish interval :" + MaxPublishInterval);
		manager.addProviders(
						new MockMarketData("M1",MaxExecutors,MaxPublishInterval,MaxPublish),
						new MockMarketData("M2",MaxExecutors,MaxPublishInterval,MaxPublish), 
						new MockMarketData("M3",MaxExecutors,MaxPublishInterval,MaxPublish));
		
		
		try (MarketDataService service = new MarketDataService(manager)) {	
			service.startPublisher();
			TimeUnit.SECONDS.sleep(1);
		    service.subscribeForInstrument(Instrument.EURGBP, Instrument.EURUSD,Instrument.USDCAD);
			TimeUnit.MINUTES.sleep(20);
		} catch (final Exception ex) {
			System.out.println("Not expected for this test :" + ex );
		}		
		System.exit(0);
	}
	
}

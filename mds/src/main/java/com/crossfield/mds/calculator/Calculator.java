package com.crossfield.mds.calculator;

import com.crossfield.mds.domain.Rate;


public interface Calculator {
	
	void calculate(final Rate r);
}

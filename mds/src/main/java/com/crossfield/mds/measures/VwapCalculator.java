package com.crossfield.mds.measures;

import java.util.Map;




import com.crossfield.mds.calculator.Calculator;
import com.crossfield.mds.domain.CcyPairUpdate;
import com.crossfield.mds.domain.Instrument;
import com.crossfield.mds.domain.Rate;
import com.lmax.disruptor.EventHandler;

/**
 * Calculates Vwap measure in response to handling a rate change event
 *
 */
public class VwapCalculator implements Calculator, EventHandler<CcyPairUpdate> {

	private final Instrument instrument;
	private double cumBidPriceVol = 0;
	private double cumBidVol = 0;

	private double cumAskPriceVol = 0;
	private double cumAskVol = 0;
	private final Map<Instrument,Measure> results;

	public VwapCalculator(final Map<Instrument,Measure> results,final Instrument instrument) {
		this.instrument = instrument;
		this.results = results;
	}
	
	public void onEvent(CcyPairUpdate event, long sequence, boolean endOfBatch){
		calculate(event.getRate());
		
		results.put(this.instrument, new Measure(getBidVWAP(), getAskVWAP()));		
    }
	
    @Override
	public void calculate(final Rate r){
		add(r.getBid(),r.getBidVol(),r.getAsk(),r.getAskVol());
	}
	
	private void add(final double bidPrice, final double bidVolume,
			final double askPrice, final double askVolume) {
		cumAskVol+=askVolume;
		cumAskPriceVol = cumAskPriceVol + (askVolume * askPrice);
		cumBidVol+= bidVolume;
		cumBidPriceVol = cumBidPriceVol + (bidVolume * bidPrice);
	}

	public double getBidVWAP() {
		return cumBidPriceVol / cumBidVol;
	}

	public double getAskVWAP() {
		return cumAskPriceVol / cumAskVol;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Vwap [instrument=").append(instrument);
		sb.append("bidVWAP=").append(getBidVWAP());
		sb.append("askVWAP=").append(getAskVWAP()).append("]");
		return sb.toString();
	}

}

package com.crossfield.mds.measures;


public final class Measure {

	private double[] measureArray;
	
	public Measure(final double... measures){
		int i =0;
		measureArray = new double[measures.length];
		for(final double d:measures){
		    measureArray[i++]=d;	
		}
	}
	
	
	public double get(final short idx){
		return measureArray[idx];
	}
}

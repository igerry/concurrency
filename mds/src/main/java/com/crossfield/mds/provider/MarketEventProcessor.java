package com.crossfield.mds.provider;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;





import com.crossfield.mds.domain.CcyPairUpdate;
import com.crossfield.mds.domain.Instrument;
import com.crossfield.mds.domain.Rate;
import com.lmax.disruptor.BatchEventProcessor;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;


/**
 * Safely publish/consume market rate change <class>Rate</class> 
 * Implemented using Disruptor pattern (a RingBuffer)
 */
public class MarketEventProcessor implements Processor,AutoCloseable {

	private final Disruptor<CcyPairUpdate> disruptor;
	private final EventHandler<CcyPairUpdate> calculator;
	private final Instrument instrument;
	// Size large to allow for slower consumption in during activity burst. Need to tune with prod data
	private final int bufferSize = 1024;  
	private BatchEventProcessor<CcyPairUpdate> eventProcessor;
    private String provider;
    


	public MarketEventProcessor(final Instrument instrument, EventHandler<CcyPairUpdate> calculator, final EventFactory<CcyPairUpdate> factory, final Executor executor){
		this.instrument= instrument;	        
		this.calculator=calculator;
		this.disruptor = new Disruptor<>(factory, bufferSize, executor);
	}


	public void init(){
		disruptor.start();
		final RingBuffer<CcyPairUpdate> buf = disruptor.getRingBuffer();
		// Register calculator as consumer
		eventProcessor = 
				new BatchEventProcessor<>(buf, buf.newBarrier(), calculator);
				buf.addGatingSequences(eventProcessor.getSequence());
				// Each EventProcessor can run on a separate thread
				final ExecutorService subExecutor = Executors.newCachedThreadPool();
				subExecutor.execute(eventProcessor);
	}


	private static final EventTranslatorOneArg<CcyPairUpdate, Rate> TRANSLATOR =
			new EventTranslatorOneArg<CcyPairUpdate, Rate>(){
		   public void translateTo(CcyPairUpdate event, long sequence, Rate rate){
			  event.setRate(rate);
		   }
	};


	public void onRateChange(final Rate rateUpdate){
		disruptor.getRingBuffer().
		   publishEvent(TRANSLATOR, rateUpdate);
	}


	public void setProvider(String provider) {
		this.provider = provider;
	}

	
	@Override
	public String toString(){
		final StringBuilder sb = new StringBuilder();
		sb.append(provider).append(" ").append(instrument.toString());
		return sb.toString();
	}


	@Override
	public void close() throws Exception {
		if(eventProcessor!=null){
	       eventProcessor.halt();
		}

	}
}
package com.crossfield.mds.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import com.crossfield.mds.domain.CcyUpdateEventFactory;
import com.crossfield.mds.domain.Instrument;
import com.crossfield.mds.measures.VwapCalculator;


/**
 * Manage the collection of market data tick providers
 *
 */
public class ProviderManager {

	private final List<MarketProvider> providers = new ArrayList<>();

	
	public void addProviders(MarketProvider... marketProviders) {
		for (final MarketProvider provider : marketProviders) {
			providers.add(provider);
		}
	}

	
	/**
	 * For each specified instrument subscribe to available MarketProvider
	 * @param instruments
	 */
	public void subscribe(final MarketEventProcessor processor,final Instrument... instruments) {
	
		for (final Instrument instrument : instruments) {
			for (final MarketProvider provider : providers) {
				provider.subscribe(instrument, processor); 
			}
		}
		
	}


	public void unsubscribeAll(final Instrument... instruments) {
			
		for (final Instrument instrument : instruments) {
			for (final MarketProvider provider : providers) {
		        provider.unsubscribe(instrument); 
			}
		}
	}
}

package com.crossfield.mds.provider;

import com.crossfield.mds.domain.Instrument;


public interface MarketProvider {

   void subscribe(Instrument instrument, Processor processor);

   void unsubscribe(Instrument instrument);
}

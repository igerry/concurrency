package com.crossfield.mds.provider;

import com.crossfield.mds.domain.Rate;

public interface Processor {

	public void onRateChange(Rate rateUpdate);
}

package com.crossfield.mds.publisher;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.crossfield.mds.domain.Instrument;
import com.crossfield.mds.measures.Measure;

public class PrintWriterPublisher implements Publisher {

	
	private  Map<Instrument,Measure> results;
	
	public PrintWriterPublisher(final Map<Instrument,Measure> results){
		
	    this.results=results;	
	}
	
	
	@Override
	public void publish() {
		//Instrument.EURGBP, Instrument.EURUSD,Instrument.USDCAD
		publishInstrument(results.get(Instrument.EURGBP));
		publishInstrument(results.get(Instrument.EURUSD));
		publishInstrument(results.get(Instrument.USDCAD));
	}

	private void publishInstrument(final Measure m){
		if (m!=null){
		    StringBuilder sb = new StringBuilder(350);
		    sb.append("VwapCalculator [instrument=").append(Instrument.EURGBP);
		    sb.append(" bidVWAP=").append(m.get((short)0));    
		    sb.append(" askVWAP=").append(m.get((short)1)).append("]");
		    sb.append(" ResultsMap sz=").append(results.size());
            System.out.println(sb.toString());
		}
	}
}

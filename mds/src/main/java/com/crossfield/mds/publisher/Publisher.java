package com.crossfield.mds.publisher;

public interface Publisher {

	void publish();
}

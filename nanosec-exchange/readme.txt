Prototype notes
--------------------------------------------------------------------------------------------------------

I've reused code I'd previously written on publication using the LMAX
Disruptor pattern.  Hence the package names contain mds.

I've focused on the low latency order processing parts rather than the less data load parts and a well formed DAO layer.

I wanted to experiment with off heap data structure, which may have over complicated this prototype.

- JVM
   - Use a VM which supports Real Time Java (eg WebSphere RT)?


Design considerations
  - Use of off-heap structures to minimise GC unpredictable pauses 
    (assumption: a small 1-2ms pause could lead to a price event processing within expected time bound).
   
  
Data
   - Trade data is less volatile than price data, so, insertion time of trade order is not as important as timely processing of price event.
   

Implementation
   - Using Spring @Autowiring which I probably wouldn't in a production application.
   - I would add logging using the Apache Log4j 2 which is also using asynchronous loggers based on the LMAX Disruptor library 
     (but not bothering for prototype)
   

To build and run (requires maven)

 Run from the project directory (./nanosec-exchange)
 mvn -DENV=test install assembly:single
 java -DENV=test -cp target\nanosec-exchange-0.0.1-SNAPSHOT-jar-with-dependencies.jar   com.crossfield.mds.server.ExchangeRunner
package com.crossfield.mds.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.when;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import com.crossfield.mds.collection.ImmortalOrderRepository;
import com.crossfield.mds.domain.Order;
import com.crossfield.mds.loader.DataLoader;


public class ImmortalOrderRepositoryTest {

	@Mock
	private DataLoader<Order> ordersMock;	
	private List<Order> testOrders = createTestOrder();
		
	private ImmortalOrderRepository repo;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		repo=new ImmortalOrderRepository();
	}

	@Test
	public void canRetriveLoadedTrades() {
		when(ordersMock.load()).thenReturn(testOrders);
		assertEquals(2, repo.load(ordersMock));
		assertEquals(1, repo.find(1).getPrice(), 0.001);
		assertEquals('B', repo.find(1).getSide());
		assertEquals(1, repo.find(1).getMargin(),0.001);
		
		assertEquals(2, repo.find(2).getPrice(), 0.001);
		assertEquals('S', repo.find(2).getSide());
		assertEquals(2, repo.find(2).getMargin(),0.001);
	}

	private List<Order> createTestOrder(){
		List<Order> ords = new ArrayList<>();
		ords.add(new Order(1,'B',1.0,1.0));
		ords.add(new Order(2,'S',2.0,2.0));
		return ords;
	}
}
package com.crossfield.mds.domain;

import static org.junit.Assert.*;
import org.junit.Test;

public class PriceTest {
	

	@Test
	public void whenSameDate() {
		assertEquals(0, new Price("2014-01-01 17:42:18", 0, 0).compareTo(new Price("2014-01-01 17:42:18", 0, 0)));
	}

	
	@Test
	public void whenDifferentDatesMostRecentIsLast() {
		assertEquals(1, new Price("2014-01-01 17:42:19", 0, 0).compareTo(new Price("2014-01-01 17:42:18", 0, 0)));
	}

}

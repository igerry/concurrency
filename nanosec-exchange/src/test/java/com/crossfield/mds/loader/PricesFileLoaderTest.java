package com.crossfield.mds.loader;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationServer.xml"})
public class PricesFileLoaderTest {

	@Autowired
	private PricesFileLoader loader;
	private Resource f = new ClassPathResource("test-prices.csv");
	

	@Test
	public void allPricesAreLoadedFromFile() {
		try {
			assertEquals("Not all rows loaded",10,loader.parse(f.getFile()).size());
		} catch (IOException ex) {
			ex.printStackTrace();
			fail("loader unexpected failure");
		}
	}

	

}

package com.crossfield.mds.loader;

import static org.junit.Assert.*;

import java.io.IOException;



import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.crossfield.mds.loader.OrdersFileLoader;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationServer.xml"})
public class OrdersFileLoaderTest {

	@Autowired
	private OrdersFileLoader loader;
	private Resource f = new ClassPathResource("trades.csv");
	

	@Test
	public void allOrdersAreLoadedFromFile() {
		try {
			assertEquals("Not all rows loaded", 1964,loader.parse(f.getFile()).size());
		} catch (IOException ex) {
			ex.printStackTrace();
			fail("Order loader unexpected failure");
		}
	}

}

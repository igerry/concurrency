package com.crossfield.mds.matching;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.crossfield.mds.domain.Order;
import com.crossfield.mds.domain.OrderType;
import com.crossfield.mds.domain.Price;
import com.crossfield.mds.service.LiquidationService;
import com.crossfield.mds.service.Liquidator;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class StopLossOrderMatchingTest {
	

	private StopLossOrderMatching matcher;
	private final static double TestRate0=1.0;
	private final static double TestRate1=1.5;
	private final static double TestRate2=2.0;
	private final static List<Long> TestSet0 = Arrays.asList(new Long[]{new Long(1)});
	private final static List<Long> TestSet1 = Arrays.asList(new Long[]{new Long(21),new Long(22) });
    private final static List<Long> TestSet2 = Arrays.asList(new Long[]{new Long(251),new Long(252),new Long(253) }) ;
	
	
	@Mock
	private LiquidationInstruction instruction;
	
	private Liquidator liquidator = new LiquidationService();
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
        matcher=new StopLossOrderMatching(OrderType.BUY, new CQEngineManagedActiveOrders(OrderType.BUY));
        addTestOrders();
	}

	@Test
	public void findsOpenBuyOrderEqualToPricePoint() {
		assertEquals(new Long(1),matcher.match(new Price("",TestRate0,TestRate0),liquidator ).getOrderId().get(0)) ;
	}

	
	@Test
	public void findsOpenBuyOrderLessThanNewPricePoint() {
		double r = TestRate0+0.0001;
		assertEquals(new Long(1),matcher.match(new Price("",r,r),liquidator ).getOrderId().get(0)) ;

	}

	@Test
	public void ignoresOpenBuyOrdersGreaterThanNewPricePoint() {
		double r = TestRate0+0.0001;
		List<Long> results=matcher.match(new Price("",r,r),liquidator ).getOrderId();
		assertTrue(results.containsAll(TestSet0));
		assertFalse(results.containsAll(TestSet1));
		assertFalse(results.containsAll(TestSet2));
	}

	

	private void addTestOrders(){
		matcher.load(new Order(1, 'B', TestRate0, 0));
		matcher.load(new Order(21, 'B', TestRate1, 0));
		matcher.load(new Order(22, 'B', TestRate1, 0));
		matcher.load(new Order(251, 'B', TestRate2, 0));
		matcher.load(new Order(252, 'B', TestRate2, 0));
		matcher.load(new Order(253, 'B', TestRate2, 0));
	
	}

}

package com.crossfield.mds.matching;

import static org.junit.Assert.*;

import java.util.Arrays;


import org.junit.Before;
import org.junit.Test;


import com.crossfield.mds.loader.OrdersFileLoader;


import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.crossfield.mds.domain.Price;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationServer.xml"})
public class SellSideStopLossOrderMatchingTest {
	

	@Autowired
	private SellSideStopLossOrderMatcher matcher;
	
	@Autowired
	private OrdersFileLoader loader;
	
	
	
	private Resource f = new ClassPathResource("sell-test-trades");
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		matcher.load(loader.parse(f.getFile()));
	}

	@Test
	public void shouldSellTradeEqualToAskPriceEvent() {
		Price testPrice = new Price("dummyPrice", 0, 760.29149);
		assertEquals("Sell order matched ask price",Arrays.asList(new Long[]{new Long(1)}), matcher.match(testPrice).getOrderId());
	}
	
	
	@Test
	public void shouldSellTradeGreaterThanAskPriceEvent() {
		Price testPrice = new Price("dummyPrice", 0, 712.19966);
		assertEquals("Sells should matched ask price",Arrays.asList(new Long[]{new Long(1),new Long(2),new Long(3)}), matcher.match(testPrice).getOrderId()  );
	}

	@Test
	public void orderIsRemovedOnceLiquidated() {
		
		Price testPrice = new Price("dummyPrice", 0, 760.29149);
		assertEquals("Sell order matched ask price",Arrays.asList(new Long[]{new Long(1)}), matcher.match(testPrice).getOrderId());
		assertEquals("Sell order should have been removed as liquidated",Arrays.asList(new Long[]{}), matcher.match(testPrice).getOrderId());
	}
}

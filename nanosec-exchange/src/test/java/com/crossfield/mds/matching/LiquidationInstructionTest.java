package com.crossfield.mds.matching;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;



import com.crossfield.mds.domain.OrderType;
import com.crossfield.mds.domain.Price;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;




public class LiquidationInstructionTest {

	
	private LiquidationInstruction instruction;
	
	@Mock
	private Price triggerPrice;
	@Mock
	private OrderType side;
	@Mock
	private List<PricePoint> orders;
	private final double AskPrice = 1;
	private final double BidPrice = 2;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
	}

	@Test
	public void liquidatedAtPriceForBuyIsTheBidPrice() {
		instruction=new LiquidationInstruction(triggerPrice, OrderType.BUY,orders);
		when(triggerPrice.getBid())
		   .thenReturn(BidPrice);
		assertEquals(BidPrice, instruction.getLiquidatedAtPrice(),0.0001);
	}

	
	@Test
	public void liquidatedAtPriceForSellIsTheAskPrice() {
		instruction=new LiquidationInstruction(triggerPrice, OrderType.SELL,orders);
		when(triggerPrice.getAsk())
		   .thenReturn(AskPrice);
		assertEquals(AskPrice, instruction.getLiquidatedAtPrice(),0.0001);
	}

	
}

package com.crossfield.mds.measures;


import com.crossfield.mds.domain.PriceUpdate;
import com.crossfield.mds.domain.Price;
import com.crossfield.mds.matching.OrderMatcher;

import com.lmax.disruptor.EventHandler;


public class RateChangeEventHandler implements EventHandler<PriceUpdate> {

	
	private final OrderMatcher matcher;
	
	
	public RateChangeEventHandler(OrderMatcher matcher) {
	    this.matcher=matcher;   
	}
	
	
	public void onEvent(PriceUpdate event, long sequence, boolean endOfBatch){
		match(event.getRate());
    }
	
    
	public void match(final Price r){
		
    	// Handle a price change by :
    	//    finding any open orders
    	//    applying liquidation on price
		matcher.match(r);
	}
}

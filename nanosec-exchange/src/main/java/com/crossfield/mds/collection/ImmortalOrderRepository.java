package com.crossfield.mds.collection;


/**
 * Off-heap structure to hold order/trade data  
 */
import java.lang.reflect.Field;
import java.util.List;

import org.springframework.stereotype.Component;

import com.crossfield.mds.domain.Order;
import com.crossfield.mds.loader.DataLoader;

import sun.misc.Unsafe;

@Component
@SuppressWarnings("restriction") 
public class ImmortalOrderRepository {

	public static final long MaxOrders = 10 * 1000;
	private static long address;
	private static final DirectMemoryOrder flyweight = new DirectMemoryOrder();
     	

	private static final Unsafe unsafe;
	    static
	    {
	        try
	        {
	            Field field = Unsafe.class.getDeclaredField("theUnsafe");
	            field.setAccessible(true);
	            unsafe = (Unsafe)field.get(null);
	        }
	        catch (Exception e)
	        {
	            throw new RuntimeException(e);
	        }
	    }
	
	
	private static DirectMemoryOrder get(final long index){
	        final long offset = address + (index * DirectMemoryOrder.getObjectSize());
	        flyweight.setObjectOffset(offset);
	        return flyweight;
	}
	
	
	public Order find(long tradeId){
		DirectMemoryOrder order = ImmortalOrderRepository.get(tradeId);
		return new Order(order.getTradeId(),order.getSide(),order.getPrice(),order.getMargin());
	}
	
	
    @SuppressWarnings("restriction")
	public int load(final DataLoader<Order> dao){
	
    	final long requiredHeap = MaxOrders * DirectMemoryOrder.getObjectSize();
    	int loaded = 0;
	    address = unsafe.allocateMemory(requiredHeap); 
	    final List<Order> orders = dao.load();
	    
	    for (final Order order:orders){
	            DirectMemoryOrder trade = get(order.getTradeId());	 
	            trade.setTradeId(order.getTradeId());
	            trade.setPrice(order.getPrice());
	            trade.setSide(order.getSide());
	            trade.setMargin(order.getMargin());
	            loaded++;
	        }
	    
	       return loaded;
	    }
	 
  
    public void destroy(){
	    unsafe.freeMemory(address);
	}
	    
	    
        @SuppressWarnings("restriction")
	    private static class DirectMemoryOrder  {
	    		    	    
	         private static long offset = 0;
	         private static final long tradeIdOffset = offset += 8;
	         private static final long sideOffset = offset += 4;
	         private static final long marginOffset = offset += 8;
	         private static final long priceOffset = offset += 8;
	         private static final long objectSize = offset += 2;
	         private long objectOffset;
	         
	         
	         public static long getObjectSize(){
	        	 return objectSize;
	         } 
	         
	         public void setObjectOffset(final long objectOffset){
	        	 this.objectOffset = objectOffset;
	         }
	         public long getTradeId(){
	        	 return unsafe.getLong(objectOffset + tradeIdOffset);
	         }
	         
	         public void setTradeId(final long tradeId){
	        	unsafe.putLong(objectOffset + tradeIdOffset, tradeId);
	         }
	         
	         
	        public double getPrice(){
	    	   return unsafe.getDouble(objectOffset + priceOffset);
	         }
	             
	        public void setPrice(final double price){
	        	 unsafe.putDouble(objectOffset + priceOffset, price);
	        }
	       
	        public char getSide(){
	        	 return unsafe.getChar(objectOffset + sideOffset);
	        }
	         
	        public void setSide(final char side){
	        	 unsafe.putChar(objectOffset + sideOffset, side);
	         }
	        
	        public double getMargin(){
	        	 return unsafe.getDouble(objectOffset + marginOffset);
	        }
	         
	        public void setMargin(final double margin){
	        	 unsafe.putDouble(objectOffset + marginOffset, margin);
	         }
	    }

	
}

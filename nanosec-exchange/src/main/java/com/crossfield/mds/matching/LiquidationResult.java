package com.crossfield.mds.matching;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class LiquidationResult {

	private final LiquidationInstruction instruction;
	private final List<Long> processedOrders;
	
	public LiquidationResult( LiquidationInstruction instruction,final List<Long> processedOrders){
		this.instruction=instruction;
		this.processedOrders=processedOrders;
	}
	
	public LiquidationInstruction getInstruction(){
		return instruction;
	}

	public List<Long> getOrderId(){
		return new ArrayList<Long>(new TreeSet<Long>(processedOrders));
	}

	
	@Override
	public String toString(){
		return "Liquidation Result " + instruction.toString();
	}
}

package com.crossfield.mds.matching;


import java.util.List;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.crossfield.mds.collection.ImmortalOrderRepository;
import com.crossfield.mds.domain.Order;
import com.crossfield.mds.domain.OrderType;
import com.crossfield.mds.domain.Price;
import com.crossfield.mds.loader.DataLoader;
import com.crossfield.mds.service.LiquidationService;
import com.crossfield.mds.service.Liquidator;



/**
 * 
 * Provide a lookup for all orders which satisfy criteria.
 *   Notes:
 *   
 *     Collection ordered by stop loss price (start of list has highest price).
 *     
 *     This PriorityBlockingQueue object is the most possibly highly contended.  
 *     Any operations should minimise time spent in collection.
 */
@Component
public class StopLossOrderMatching   {

	@Autowired
	private ImmortalOrderRepository repo;
	
	private ExecutorService executor=Executors.newCachedThreadPool();
	@Autowired LiquidationService liquidator;
	

	private CQEngineManagedActiveOrders openOrders; 
	private OrderType orderType;

	@Bean @Qualifier("sellSideMatcher")
	public StopLossOrderMatching sellSide() {
	      return new StopLossOrderMatching(OrderType.SELL, new CQEngineManagedActiveOrders(OrderType.SELL));
	}

	@Bean @Qualifier("buySideMatcher")
	public StopLossOrderMatching buySide() {
	      return new StopLossOrderMatching(OrderType.BUY, new CQEngineManagedActiveOrders(OrderType.BUY));
	}

	
	public StopLossOrderMatching(OrderType orderType, CQEngineManagedActiveOrders orderManager){
		this.orderType=orderType;
		this.openOrders=orderManager;
	}

	public StopLossOrderMatching(){
	}

	
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public int load(final DataLoader<Order> dao){
         return load(dao.load());
     }

     public int load(final List<Order> orders){     
        int loaded = 0;
        for (final Order order:orders){
	        if (orderType.equals(OrderType.valueOf(order.getSide()))){  
	            load(order);
	            loaded++;
	        }
        }
        return loaded;
    }
     
     
    public Order load(final Order order){
    	openOrders.add(order);
   	    return order;
     }
     
 
	
    public LiquidationResult match(Price triggerPrice){
    	return match(triggerPrice, this.liquidator);
    }
 
    
   protected LiquidationResult match(Price triggerPrice, final Liquidator liquidator){
    	
    	LiquidationResult result=null;
    	try {
    		final Future<LiquidationResult> searchTask= executor.submit(
    				new OrderLiquatorTask(triggerPrice, liquidator,orderType, openOrders));  
    		result=searchTask.get();
    	} catch (InterruptedException|ExecutionException e) {
    		Thread.currentThread().interrupt();
    	} 	
    	return result;
    }
 
}

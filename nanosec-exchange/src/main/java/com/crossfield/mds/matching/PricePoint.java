package com.crossfield.mds.matching;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;

public class PricePoint implements Comparable<PricePoint> {

	private final double pricePoint;
	private final List<Long> orderId;  // Orders associated to this price point
	private final ReentrantReadWriteLock ordersLock = new ReentrantReadWriteLock();
	private final Lock writeLock = ordersLock.writeLock();
	private final Lock readLock = ordersLock.readLock();

	public PricePoint(final double pricePoint, List<Long>orderId){
		this.pricePoint=pricePoint;
		this.orderId=orderId;
	}
	
	
	public static final Attribute<PricePoint, Double> PRICEPOINT_NAME = new SimpleAttribute<PricePoint, Double>("price") {
        public Double getValue(PricePoint pp) { return pp.pricePoint; }
     };


    public List<Long> removeOrders(){
    	List<Long> ordersCopy=null;
    	try{
			writeLock.lock();
			ordersCopy=new ArrayList<Long>(orderId);
			orderId.clear();
		}finally{
	    	writeLock.unlock();
	    }
    	return ordersCopy;
    	
    }
    
	public void addOrder(final Long newId) {
		try{
			writeLock.lock();
			orderId.add(newId);	
		}finally{
	    	writeLock.unlock();
	    }
		
	}
	
	@Override
	public int compareTo(PricePoint o) {
		return Double.compare(this.pricePoint,o.pricePoint);
	}
	@Override
	public int hashCode(){
		return Long.valueOf(Double.doubleToLongBits(pricePoint)).hashCode();
	}
	@Override
	public boolean equals(Object o){
		if(o instanceof PricePoint){
			return (0==Double.compare(this.pricePoint, ((PricePoint)o).pricePoint) &&
					  orderId.equals(((PricePoint)o).orderId));
		}
		return false;
	}
	
	public double getPricePoint() {
		return pricePoint;
	}
	
	@Override
	public String toString(){
		return ""+pricePoint+ " -> " + orderId;
	}
}
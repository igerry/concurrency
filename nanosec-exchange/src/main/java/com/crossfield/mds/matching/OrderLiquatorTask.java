package com.crossfield.mds.matching;

import java.util.List;
import java.util.concurrent.Callable;

import com.crossfield.mds.domain.OrderType;
import com.crossfield.mds.domain.Price;
import com.crossfield.mds.service.Liquidator;

public class OrderLiquatorTask implements Callable<LiquidationResult>{

	private final Liquidator liquidator;
	private final ActiveOrdersManager searchable;
    private final OrderType orderType;
    private final Price trigger;
   
	
	public OrderLiquatorTask(Price trigger, Liquidator liquidator,OrderType orderType,
			ActiveOrdersManager searchable){
		this.liquidator=liquidator;
		this.searchable=searchable;
		this.orderType=orderType;
		this.trigger=trigger;
	}
	

    public List<PricePoint> search(double r){
		return searchable.search(r);
	}

	@Override
	public LiquidationResult call() throws Exception {
		/*
		 * Search through the order repository for orders and liquidate the ones found
		 */
		double rateUpdate  = orderType.equals(OrderType.BUY) ? trigger.getBid() : trigger.getAsk();
		return liquidator.liquidate(new LiquidationInstruction(trigger, orderType,search(rateUpdate)));
	}
}
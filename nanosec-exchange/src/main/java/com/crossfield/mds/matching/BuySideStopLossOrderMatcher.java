package com.crossfield.mds.matching;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.crossfield.mds.domain.Order;
import com.crossfield.mds.domain.Price;
import com.crossfield.mds.loader.DataLoader;



@Component("buyStopLoss")
public class BuySideStopLossOrderMatcher implements OrderMatcher {


	@Autowired @Qualifier("buySideMatcher")
	protected StopLossOrderMatching matcher;


	@Override
	public LiquidationResult match(Price rateUpdate) {
	     return matcher.match(rateUpdate);
	}

	@Override
	public int load(DataLoader<Order> dao) {
		return matcher.load(dao);
	}
}

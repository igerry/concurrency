package com.crossfield.mds.matching;

import java.util.List;

import com.crossfield.mds.domain.OrderType;
import com.crossfield.mds.domain.Price;


public class LiquidationInstruction{
	
	private final OrderType side;
	private final Price triggerPrice;
	private List<PricePoint> orders;

	public LiquidationInstruction(Price triggerPrice, final OrderType side, final List<PricePoint> orders){
		this.side=side;
		this.orders=orders;
		this.triggerPrice=triggerPrice;
	}

	
	public final List<PricePoint> orders(){
		return orders;
	}
	
	
	public Price getLiquidatedPrice(){
		return triggerPrice;
	}
	
	public double getLiquidatedAtPrice(){
		return side.equals(OrderType.BUY) ? triggerPrice.getBid() : triggerPrice.getAsk();
	}

}
package com.crossfield.mds.matching;

import static com.googlecode.cqengine.query.QueryFactory.greaterThanOrEqualTo;
import static com.googlecode.cqengine.query.QueryFactory.lessThanOrEqualTo;
import static com.googlecode.cqengine.query.QueryFactory.equal;

import java.util.ArrayList;
import java.util.List;


import com.crossfield.mds.domain.Order;
import com.crossfield.mds.domain.OrderType;
import com.crossfield.mds.matching.PricePoint;
import com.googlecode.cqengine.CQEngine;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.index.hash.HashIndex;
import com.googlecode.cqengine.query.Query;
import com.googlecode.cqengine.resultset.ResultSet;


public class CQEngineManagedActiveOrders implements ActiveOrdersManager {

	private final IndexedCollection<PricePoint> indexedActiveOrders;
	private final OrderType orderType;
	
	
	public CQEngineManagedActiveOrders(final OrderType orderType){
		this.orderType=orderType;
		indexedActiveOrders = CQEngine.newInstance();
		indexedActiveOrders.addIndex(HashIndex.onAttribute(PricePoint.PRICEPOINT_NAME ));
	}
	
	
	@Override
    public List<PricePoint> search(double r){
	
    	final List<PricePoint> atPricePoint=new ArrayList<>();
		final Query<PricePoint> query = orderType.equals(OrderType.BUY)?  
				               lessThanOrEqualTo(PricePoint.PRICEPOINT_NAME, new Double(r)):
				            	   greaterThanOrEqualTo(PricePoint.PRICEPOINT_NAME, new Double(r));
				               
        for (final PricePoint p : indexedActiveOrders.retrieve(query)) {
        	atPricePoint.add(p);    					            
		}
		return atPricePoint;  // All PricePoints which need to be liquidated
	}
	
	
	@Override
	public void add(final Order order){

		final Query<PricePoint> query = equal(PricePoint.PRICEPOINT_NAME, new Double(order.stopLoss()));

		final ResultSet<PricePoint> results = indexedActiveOrders.retrieve(query);

		if(results!=null&&results.size()>0){
			assert(results.size()==1);
			for(PricePoint p:results){
				p.addOrder(order.getTradeId());
			}
		}else{
			// Order is at a PricePoint which doesn't exist in collection 	
			final PricePoint newPricePoint=new PricePoint(order.stopLoss(), new ArrayList<Long>() );
			newPricePoint.addOrder(order.getTradeId());
			indexedActiveOrders.add(newPricePoint);
		}

	}
}

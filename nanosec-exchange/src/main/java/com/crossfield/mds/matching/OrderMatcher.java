package com.crossfield.mds.matching;

import com.crossfield.mds.domain.Order;
import com.crossfield.mds.domain.Price;
import com.crossfield.mds.loader.DataLoader;

/**
 * Holder of all liquidation orders
 * 
 *
 */
public interface OrderMatcher {

	public int load(final DataLoader<Order> dao);

	public LiquidationResult match(Price rateUpdate);
}

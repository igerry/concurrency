package com.crossfield.mds.matching;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedDeque;

import com.crossfield.mds.domain.OrderType;
import com.crossfield.mds.service.Liquidator;


@Deprecated
public class OrderSearcher1 implements Callable<LiquidationResult>{

	
	private final Liquidator liquidator;
	private final ConcurrentLinkedDeque<PricePoint> searchable;
    private final OrderType orderType;
    private final LiquidationInstruction liquidate;
	
	public OrderSearcher1(Liquidator liquidator, double rateUpdate, OrderType orderType,
			 ConcurrentLinkedDeque<PricePoint> searchable, LiquidationInstruction liquidate){
		this.liquidator=liquidator;
		this.searchable=searchable;
		this.orderType=orderType;
		
		this.liquidate=liquidate;
	}
	

    protected List<List<Long>> search(double r, List<List<Long>> foundOrders){
		
		final Iterator<PricePoint> iter = orderType.iterator(searchable);
	
		while(iter.hasNext()){
			final PricePoint p = iter.next();
			final int compare = Double.compare(r,p.getPricePoint());
			if(orderType.match(compare)){
			   foundOrders.add(p.removeOrders());
			}
		}
		return foundOrders;
	}

	@Override
	public LiquidationResult call() throws Exception {
		//search(rateUpdate, liquidate.orders());
		return liquidator.liquidate(liquidate);
	}
}
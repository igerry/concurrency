package com.crossfield.mds.matching;

import java.util.List;

import com.crossfield.mds.domain.Order;

public interface ActiveOrdersManager {

	void add(final Order o);
	
	List<PricePoint> search(double r);
}

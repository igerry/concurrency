package com.crossfield.mds.matching;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.crossfield.mds.domain.Order;
import com.crossfield.mds.domain.Price;
import com.crossfield.mds.loader.DataLoader;



@Component("sellStopLoss")
public class SellSideStopLossOrderMatcher implements OrderMatcher {

	@Autowired @Qualifier("sellSideMatcher")
	protected StopLossOrderMatching matcher;


	@Override
	public LiquidationResult match(Price update) {
	     return matcher.match(update);
	}

	@Override
	public int load(DataLoader<Order> dao) {
		return matcher.load(dao);
	}

	
	public int load(List<Order> orders) {
		return matcher.load(orders);
	}

}

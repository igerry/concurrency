package com.crossfield.mds.domain;

import java.text.ParseException;
import java.util.Date;


import org.apache.commons.lang3.time.DateUtils;


import com.crossfield.mds.exception.ExchangeServerException;

// Encapsulate Rate information
// mutable as Rate instances are allocated upfront to minimise GC, ie we want these object instances in immortal memory.
public class Price implements Comparable<Price> {
	
	private static final String ParsePattern="yyyy-MM-dd HH:mm:ss";
	private String timestamp;
	private double bid;
	private double ask;

	
	public Price(final String timestamp, final double bid, final double ask) {
		this.timestamp=timestamp;
		this.bid = bid;
		this.ask = ask;
	}

	public String getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(final String timestamp) {
		this.timestamp=timestamp;
	}

	public void setBid(final double bid) {
		this.bid=bid;
	}
	
	public double getBid() {
		return bid;
	}

	public double getAsk() {
		return ask;
	}

	public void setAsk(double ask) {
	    this.ask=ask;
	}

	
	@Override
	public int compareTo(Price other) {
		return parse(this.timestamp).compareTo(parse(other.timestamp));
	}
	
	private Date parse(String dateString){
		Date d = null;
		try {
			d=DateUtils.parseDate(dateString, ParsePattern);
		} catch (final ParseException ex) {
			throw new ExchangeServerException("Failed whilst parsing a Price date string ["+dateString+"]" + " REASON ["+ex.getMessage()+"]");
		}
		return d;
		
	}
	
	@Override 
	public int hashCode(){
		return timestamp.hashCode();
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof Price){
			final Price p=(Price)o;
			return timestamp.equals(p.timestamp) &&
					 bid == p.bid &&
					 ask == p.ask;
		}
		return false;
	}
		
	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder(200);
		sb.append("[Price ");
		sb.append("at ").append(timestamp);
		sb.append(", ask=").append(ask);
		sb.append(", bid=").append(bid);
		sb.append("]");
		return sb.toString();
	}

	
	

}

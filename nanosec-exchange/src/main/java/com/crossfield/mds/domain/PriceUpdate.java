package com.crossfield.mds.domain;


public class PriceUpdate {

	private Price rate;
		
	public PriceUpdate(){
		rate = new Price("", 0, 0);
	}
	public Price getRate() {
		return rate;
	}

	public void setRate(final Price nrate) {
		rate.setTimestamp(nrate.getTimestamp());
		rate.setAsk(nrate.getAsk());
		rate.setBid(nrate.getBid());
	}
}

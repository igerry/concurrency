package com.crossfield.mds.domain;

import java.util.Deque;
import java.util.Iterator;

import com.crossfield.mds.matching.PricePoint;

// OrderType static factory
public abstract class OrderType {
	
	
	public static final OrderType BUY = new BuyOrder();
	public static final OrderType SELL = new SellOrder();
	
	public abstract boolean match(final int compare);

	public abstract Iterator<PricePoint> iterator(Deque<PricePoint> queue);

	
	public static OrderType valueOf(Character c){
		return(BuyOrder.BUY.equals(c.toString())) ? BUY:SELL;
    }
	   
	
	public static class BuyOrder extends OrderType{
		
		public final static String BUY="B";
	
		
		private BuyOrder(){}
		
		@Override
		public boolean match(final int compare){
		    return compare<0||compare==0;
		}
		
		@Override
		public Iterator<PricePoint> iterator(Deque<PricePoint> queue){
			return queue.iterator();
		}
		@Override 
		public int hashCode(){
			return 1;
		}	
		
	}

	public static class SellOrder extends OrderType{
		
		public  final static String SELL="S";
		
		
		private SellOrder(){}
		
		@Override
		public boolean match(final int compare){
		    return (compare<0||compare==0);
		}	
	
		@Override
		public Iterator<PricePoint> iterator(Deque<PricePoint> queue){
			return queue.descendingIterator();
		}
		@Override 
		public int hashCode(){
			return 2;
		}

	}
}

package com.crossfield.mds.domain;

import com.lmax.disruptor.EventFactory;



public class UpdateEventFactory implements EventFactory<PriceUpdate> {
	
	public static final UpdateEventFactory INSTANCE = new UpdateEventFactory();

	public PriceUpdate newInstance(){
	    return new PriceUpdate();
	 }
}

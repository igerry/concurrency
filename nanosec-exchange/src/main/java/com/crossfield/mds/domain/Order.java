package com.crossfield.mds.domain;

public class Order {
	
	private final long tradeId;
	private final char side;
	private final double price;
	private final double margin;
	
	public Order(final long tradeId, final char side, final double price, final double margin){
		this.tradeId=tradeId;
		this.side=side;
		this.price=price;
		this.margin=margin;
		
	}
	
	@SuppressWarnings("unused")
	private Order(){
		this(0,(char)0,0,0);
	}
	
    public long getTradeId(){
   	 return tradeId;
    }
    
    
   public double getPrice(){
	   return price;
    }

   public double getMargin(){
	   return margin;
    }

   public double stopLoss(){
	   return OrderType.BUY.equals(OrderType.valueOf(side)) ? price - margin : price + margin;
   }
   
   public char getSide(){
   	 return side;
   }
    
  

}
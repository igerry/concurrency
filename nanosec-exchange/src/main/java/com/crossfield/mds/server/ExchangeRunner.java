package com.crossfield.mds.server;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class ExchangeRunner {
	
   public static void main(String[] args) throws Exception{
      
	   ApplicationContext context = null;
	   ExchangeService service = null;
	   
	   try{
	     context = 
             new ClassPathXmlApplicationContext("applicationServer.xml");
          service = (ExchangeService) context.getBean("exchange");
          service.start();
	   }catch(final Exception ex){
		   ex.printStackTrace();
	   }finally{
		   if(service!=null){
		       service.stop();
		   }   
	   }
   }
}

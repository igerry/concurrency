package com.crossfield.mds.server;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.crossfield.mds.collection.ImmortalOrderRepository;
import com.crossfield.mds.exception.ExchangeServerException;
import com.crossfield.mds.loader.OrdersFileLoader;
import com.crossfield.mds.loader.PricesFileLoader;
import com.crossfield.mds.matching.OrderMatcher;
import com.crossfield.mds.service.MarketDataService;
import com.crossfield.mds.service.Service;

@Component
public class ExchangeService implements Service {
	
	
	@Autowired
	private OrdersFileLoader orders;
	@Autowired
	private PricesFileLoader pricesLoader; 
	@Autowired 
	private MarketDataService marketDataService;
	@Autowired
	private ImmortalOrderRepository orderRepository;
    @Autowired @Qualifier("sellStopLoss")
	private OrderMatcher buyMatcher;
    @Autowired @Qualifier("buyStopLoss")
	private OrderMatcher sellMatcher;
    
	@Value("${dataInputPath}")
	private String dataInputPath;
	@Value("${ordersFileName}")
	private String ordersFileName;


	
    @Override
	public void start() {    	
    	try{
    	  restoreState();	
    	  System.out.println("Exchange OPEN");
		  marketDataService.start();
		  TimeUnit.SECONDS.sleep(60);
		  marketDataService.stop();
		  System.exit(0);
    	}catch(Exception ex){
    		ex.printStackTrace();
    		throw new ExchangeServerException("Exchange Server error during startup");
    	}
	}
	
	
    @Override
	public void stop() throws Exception {
		marketDataService.unsubscribeAll();
	}
	
	
    protected void restoreState(){
  	    orders.setFilename(dataInputPath+ordersFileName);	
  	    orderRepository.load(orders);   // Load open Orders from file to Cache
  	    buyMatcher.load(orders);
  	    sellMatcher.load(orders);
    }
}

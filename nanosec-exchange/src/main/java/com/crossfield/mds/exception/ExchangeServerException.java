package com.crossfield.mds.exception;


public class ExchangeServerException extends RuntimeException {
	private static final long serialVersionUID = -4172448355214910954L;
	
	 /**
     * @param cause
     */
	public ExchangeServerException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param string
	 */
	public ExchangeServerException(String string) {
		super(string);
	}

	/**
	 * @param string
	 * @param cause
	 */
	public ExchangeServerException(String string, Throwable cause) {
		super(string, cause);
	}
}
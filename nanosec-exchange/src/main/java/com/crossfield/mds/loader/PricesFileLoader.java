package com.crossfield.mds.loader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.crossfield.mds.domain.Price;
import com.crossfield.mds.exception.ExchangeServerException;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Quick and dirty data loader for parsing the Prices file.
 * For the prototype these are stored on-heap, in actual implementation these would be streamed to exchange.
 */
@Component
public class PricesFileLoader implements DataLoader<Price>{
	
	@Value("${pricesFileName}")
	private String filename;
	

	@Override
	 public List<Price> load(){
		System.out.println("DATA LOAD  " + filename +" (this takes a while)");
	   	return parse(new File(filename));
	}

	public List<Price> parse(File f){
	        
		 final Set<Price> loadedPrice = new TreeSet<>();
		 
		 try(CSVReader reader = new CSVReader(new FileReader(f), ',')){
	        String[] record = null;	         
	        while((record = reader.readNext()) != null){
	            loadedPrice.add( new Price(record[0], 
	            		           Double.parseDouble(record[1]), 
	            		           Double.parseDouble(record[2]))); 
	        }
		 }catch (final IOException ex){   
			 ex.printStackTrace();
	         throw new ExchangeServerException("Prototype can't load the order file from " + filename);   
		 }
		 List<Price> ordered = new ArrayList<>(loadedPrice);
		 System.out.println("DATA LOAD Prices " + filename + " DONE");
	     return ordered;
	 }
	 
	 
     public String getFilename() {
			return filename;
	 }
     
     public void setFilename(String fileName) {
  	    this.filename=fileName;
  	 }


}

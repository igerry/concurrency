package com.crossfield.mds.loader;

import java.util.List;

public interface DataLoader<T> {
	
     List<T> load();
}

package com.crossfield.mds.loader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.crossfield.mds.domain.Order;
import com.crossfield.mds.exception.ExchangeServerException;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Quick and dirty data loader for parsing the Orders file (aka trades file)
 *
 */
@Component
public class OrdersFileLoader implements DataLoader<Order>{

	@Value("${ordersFileName}")
	private String filename;
	

	@Override
	 public List<Order> load(){
		System.out.println("DATA LOAD Orders " + filename);
	   	return parse(new File(filename));
	}

	public List<Order> parse(File f){
	        
		 final List<Order> emps = new ArrayList<Order>(2000);
		 
		 try(CSVReader reader = new CSVReader(new FileReader(f), ',')){
	         
	        
	        String[] record = null;	         
	        while((record = reader.readNext()) != null){
	        	// Example input row  1,"Buy",673.04696,60.0000
	            emps.add( new Order(Long.parseLong(record[0]), 
	            		           side(record[1]),
	            		           Double.parseDouble(record[2]), 
	            		           Double.parseDouble(record[3])));  
	        }
		 }catch (final IOException ex){   
			 ex.printStackTrace();
	         throw new ExchangeServerException("Prototype can't load the order file from " + filename);   
		 }
		 System.out.println("DATA LOAD Orders " + filename + " DONE");
	     return emps;
	 }
	 
	 private char side(final String sideName){
		 char side;
		 if ("Buy".equals(sideName)){
			 side='B';
		 }else{
			 side='S';
		 }
		 return side;
	 }
	 
	 
     public String getFilename() {
			return filename;
	 }
     
     public void setFilename(String fileName) {
  	    this.filename=fileName;
  	 }


}

package com.crossfield.mds.service;

public interface Service {

	public void start();
	
	public void stop() throws Exception;
}

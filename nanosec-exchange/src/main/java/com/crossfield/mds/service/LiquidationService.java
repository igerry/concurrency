package com.crossfield.mds.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.crossfield.mds.domain.Price;
import com.crossfield.mds.matching.LiquidationInstruction;
import com.crossfield.mds.matching.LiquidationResult;
import com.crossfield.mds.matching.PricePoint;

/**
 * Order needs to be liquidated/executed at the market.  
 * In the prototype we write a String ;
 * 
 * @author igerry
 *
 */
@Component
public class LiquidationService implements Liquidator {

	
	
	@Override
	public LiquidationResult liquidate(LiquidationInstruction instruction) {
		final List<Long> liquidated = new ArrayList<>();
		// Liquidate active orders	
		//9 <timestamp of price change> 679 (note 679 is below 680)
		final Price price = instruction.getLiquidatedPrice();
		for(final PricePoint ordersAtPrice : instruction.orders()){
		    for (final Long orderId:ordersAtPrice.removeOrders()){
		    	liquidated.add(orderId);
			    System.out.println(String.format("%d %s %s",orderId,price.getTimestamp(),String.valueOf(instruction.getLiquidatedAtPrice() )));
		   }
		}
		return new LiquidationResult(instruction,liquidated);
	}

}

package com.crossfield.mds.service;



import com.crossfield.mds.matching.LiquidationInstruction;
import com.crossfield.mds.matching.LiquidationResult;

public interface Liquidator {

	 public  LiquidationResult  liquidate(LiquidationInstruction instruction);
	 
}

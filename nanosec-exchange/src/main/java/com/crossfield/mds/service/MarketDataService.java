package com.crossfield.mds.service;


import java.util.Arrays;
import java.util.concurrent.Executors;






import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;






import com.crossfield.mds.domain.UpdateEventFactory;
import com.crossfield.mds.matching.BuySideStopLossOrderMatcher;
import com.crossfield.mds.matching.SellSideStopLossOrderMatcher;
import com.crossfield.mds.measures.RateChangeEventHandler;
import com.crossfield.mds.provider.MarketEventProcessor;
import com.crossfield.mds.provider.MarketProvider;
import com.crossfield.mds.provider.ProviderManager;



/**
 * Associate providers with business logic processors
 */
@Component
public class MarketDataService implements Service {

	@Autowired
	private ProviderManager manager;
	@Autowired
	private MarketProvider pricesProvider;
	@Autowired
	private BuySideStopLossOrderMatcher buyOrderMatcher; 
	@Autowired
	private SellSideStopLossOrderMatcher sellOrderMatcher; 	
	
	
	public void subscribe(){
		manager.addProviders(pricesProvider);
        manager.subscribe(createProcessor());	
	}
	
	
	public void unsubscribeAll(){
		if(manager!=null){
		    manager.unsubscribeAll();
		}
	}

	private MarketEventProcessor createProcessor(){
		
		final MarketEventProcessor.OrderProcessorBuilder builder = new  MarketEventProcessor.OrderProcessorBuilder();
		builder.setExecutor(Executors.newCachedThreadPool());
		builder.setFactory(new UpdateEventFactory());
		builder.setLiquidator(Arrays.asList(
				new RateChangeEventHandler[]{  new RateChangeEventHandler(buyOrderMatcher), 
						                       new RateChangeEventHandler(sellOrderMatcher)}) );
		return builder.build();
	}
	
	@Override
	public void start(){
	   subscribe();
	}
	
	@Override
	public void stop()  {
		// Do some sensible clean-up and production alerting
		unsubscribeAll();
	}
	
	public void setManager(ProviderManager manager) {
		this.manager = manager;
	}
}

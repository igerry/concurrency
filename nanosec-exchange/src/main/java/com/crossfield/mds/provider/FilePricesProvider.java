package com.crossfield.mds.provider;


import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.crossfield.mds.domain.Price;
import com.crossfield.mds.loader.PricesFileLoader;
import com.crossfield.mds.provider.MarketProvider;
import com.crossfield.mds.provider.Processor;

/**
 * Simulates market prices arriving at random intervals into the Exchange.
 * @author igerry
 *
 */

@Component
public class FilePricesProvider implements MarketProvider,InitializingBean {

	@Autowired
	private PricesFileLoader loader;
	@Value("${dataInputPath}")
	private String dataInputPath;
	@Value("${pricesFileName}")
	private String pricesFileName;
	@Value("${priceProvider.maxExecutorSz}") 
	private int maxExecutorSz;
	@Value("${priceProvider.publishInterval}") 
	private long publishInterval;	
	private CopyOnWriteArrayList<Price> prices;
	private ScheduledExecutorService executor;

   
	@Override
	public void afterPropertiesSet() throws Exception {
		this.executor = Executors.newScheduledThreadPool(maxExecutorSz);	
	}
	
	@Override
	public void subscribe(final Processor processor) {
		
		loader.setFilename(dataInputPath+pricesFileName);	
		this.prices = new CopyOnWriteArrayList<Price>(loader.load());
	
		System.out.println(
				String.format("Subscription to prices provider started"));
		Iterator<Price> iter = prices.iterator();
		while(iter.hasNext()){
		    executor.schedule(createRateTickTask(processor,iter.next() ),
				createDelay(), TimeUnit.MILLISECONDS);
		}
		System.out.println(
				String.format("Prices published"));
	}
	
	
	@Override
	public void unsubscribe() {
		 executor.shutdown();  
		 System.out.println(
					String.format("Subscription stopped to prices provider stopped"));
	}


	private Runnable createRateTickTask(final Processor processor, final Price p) {
		return new Runnable() {
			@Override
			public void run() {
				   processor.onRateChange(p);
				   executor.schedule(this, createDelay(), TimeUnit.MILLISECONDS);
		    }
		};
	}

	private long createDelay() {
		return (long) (Math.random() * publishInterval);

	}



}

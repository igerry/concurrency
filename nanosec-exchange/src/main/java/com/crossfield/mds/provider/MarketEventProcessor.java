package com.crossfield.mds.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.crossfield.mds.domain.PriceUpdate;
import com.crossfield.mds.domain.Price;
import com.crossfield.mds.measures.RateChangeEventHandler;
import com.lmax.disruptor.BatchEventProcessor;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;


/**
 * Safely publish/consume market rate change <class>Rate</class> 
 * Implemented using IMAX Disruptor pattern (a RingBuffer)
 * 
 * 
 */
public class MarketEventProcessor implements Processor,AutoCloseable {

	private final Disruptor<PriceUpdate> disruptor;
	private final List<RateChangeEventHandler> liquidators;
	private List<BatchEventProcessor<PriceUpdate>> eventProcessors;    // Consumers for the price change events

    
  
    public static class OrderProcessorBuilder {
    	
    	private Disruptor<PriceUpdate> disruptor;
    	private List<RateChangeEventHandler> liquidators;
		private EventFactory<PriceUpdate> factory;
    	private Executor executor;
    	private final int bufferSize = 1024;  // Size large to allow for slower consumption in during activity burst. Need to tune with prod data
	
		public void setLiquidator(final List<RateChangeEventHandler> liquidators) {
			this.liquidators = liquidators;
		}
		
		public void setFactory(EventFactory<PriceUpdate> factory) {
			this.factory = factory;
		}
		
		public void setExecutor(Executor executor) {
			this.executor = executor;
		}
    	
		public MarketEventProcessor build(){
			disruptor = new Disruptor<>(factory, bufferSize, executor);
			MarketEventProcessor processor = new MarketEventProcessor(this);
			processor.init();
			return processor;
		}
    }

    
    private MarketEventProcessor (OrderProcessorBuilder builder){
		this.liquidators=builder.liquidators;
		this.disruptor = builder.disruptor;
    }
    

    // Register consumers of price event
	private void  init(){
		
		disruptor.start();
		eventProcessors = new ArrayList<>(); 
		final RingBuffer<PriceUpdate> buf = disruptor.getRingBuffer();
		for(final EventHandler<PriceUpdate> handler:liquidators){
			final BatchEventProcessor<PriceUpdate> processor = new BatchEventProcessor<>(buf, buf.newBarrier(), handler);
		    eventProcessors.add(processor);	
		    buf.addGatingSequences(processor.getSequence());
			Executors.newCachedThreadPool().execute(processor);
		}
	}


	private static final EventTranslatorOneArg<PriceUpdate, Price> TRANSLATOR =
			new EventTranslatorOneArg<PriceUpdate, Price>(){
		   public void translateTo(PriceUpdate event, long sequence, Price rate){
			  event.setRate(rate);
		   }
	};


	public void onRateChange(final Price rateUpdate){
		disruptor.getRingBuffer().
		   publishEvent(TRANSLATOR, rateUpdate);
	}


	@Override
	public void close() throws Exception {
		if(eventProcessors!=null){
			for(final BatchEventProcessor<PriceUpdate> ep :   eventProcessors){
				ep.halt();	
			}
		}
	}
}
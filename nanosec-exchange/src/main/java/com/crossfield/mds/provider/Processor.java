package com.crossfield.mds.provider;

import com.crossfield.mds.domain.Price;

public interface Processor {

	public void onRateChange(Price rateUpdate);
}

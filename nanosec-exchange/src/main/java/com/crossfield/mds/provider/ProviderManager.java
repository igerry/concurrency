package com.crossfield.mds.provider;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;


/**
 * Manage the collection of market data tick providers
 *
 */

@Component
public class ProviderManager {

	private final List<MarketProvider> providers = new ArrayList<>();

	
	public void addProviders(MarketProvider... marketProviders) {
		for (final MarketProvider provider : marketProviders) {
			providers.add(provider);
		}
	}

	
	/**
	 * For each specified instrument subscribe to available MarketProvider
	 * @param instruments
	 */
	public void subscribe(final MarketEventProcessor processor) {
	

	    for (final MarketProvider provider : providers) {
		    provider.subscribe(processor); 
		}		
	}


	public void unsubscribeAll() {
			
	    for (final MarketProvider provider : providers) {
		   provider.unsubscribe(); 
		}
		
	}
}

package com.crossfield.mds.provider;


public interface RateChange {

	void onRateChange(RateChange rateChange);
}

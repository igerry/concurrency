package com.crossfield.mds.provider;




public interface MarketProvider {

   void subscribe(Processor processor);

   void unsubscribe();
}
